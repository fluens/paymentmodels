<?php

namespace Fluens\PaymentModels;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property Collection $reduction_schemes
 */
class Condition extends Model
{
    public function reductionSchemes(){
        return $this->hasMany(ReductionScheme::class);
    }
}
