<?php

namespace Fluens\PaymentModels;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string method
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property Status $status
 * @property Invoice $invoice
 * @property ReductionScheme $reduction_scheme
 */
class Payment extends Model
{
    protected $fillable = ['status_id', 'invoice_id', 'method', 'reduction_scheme_id'];
    public function status(){
        return $this->belongsTo(Status::class);
    }
    public function invoice(){
        return $this->belongsTo(Invoice::class);
    }
    public function reductionScheme(){
        return $this->belongsTo(ReductionScheme::class);
    }

}
