<?php

namespace Fluens\PaymentModels;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int application_id
 * @property string name
 * @property string description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property Collection $invoice_lines
 * @property Collection $invoices
 * @property Collection $payments
 * @property Condition $condition
 * @property float reduction_factor
 */
class ReductionScheme extends Model
{
    protected $fillable = ['application_id', 'name', 'reduction_factor', 'description', 'condition_id'];
    public function invoiceLines(){
        return $this->hasMany(InvoiceLine::class);
    }
    public function invoices(){
        return $this->hasMany(Invoice::class);
    }
    public function payments(){
        return $this->hasMany(Payment::class);
    }
    public function condition(){
        return $this->belongsTo(Condition::class);
    }

}
