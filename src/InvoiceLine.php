<?php

namespace Fluens\PaymentModels;

use Illuminate\Database\Eloquent\Model;


/**
 * @property int $id
 * @property int application_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property Invoice $invoice
 * @property Status $status
 * @property ReductionScheme $reduction_scheme
 */
class InvoiceLine extends Model
{
    protected $fillable = ['application_id', 'invoice_id', 'status_id', 'reduction_scheme_id'];

    public function invoice(){
        return $this->belongsTo(Invoice::class);
    }
    public function status(){
        return $this->belongsTo(Status::class);
    }
    public function reductionScheme(){
        return $this->belongsTo(ReductionScheme::class);
    }
}
