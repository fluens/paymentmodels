<?php

namespace Fluens\PaymentModels;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property Collection $invoice_lines
 * @property Collection $invoices
 * @property Collection $payments
 * @property Status $former_status
 * @property Status $next_status
 */
class Status extends Model
{
    protected $fillable = ['name', 'description', 'former_status_id', 'next_status_id'];
    public function invoiceLines(){
        return $this->hasMany(InvoiceLine::class);
    }
    public function invoices(){
        return $this->hasMany(Invoice::class);
    }
    public function payments(){
        return $this->hasMany(Payment::class);
    }
    public function formerStatus(){
        return $this->belongsTo(Status::class, 'former_status_id');
    }
    public function nextStatus(){
        return $this->belongsTo(Status::class, 'next_status_id');
    }
}
