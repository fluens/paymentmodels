<?php

namespace Fluens\PaymentModels;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property Carbon dueDate
 * @property int recipient_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property Collection $invoice_lines
 * @property Status $status
 * @property ReductionScheme $reduction_scheme
 * @property Collection $payments
 */
class Invoice extends Model
{
    protected $fillable = ['application_id', 'due_date', 'status_id', 'reduction_scheme_id', 'recipient_id'];

    public function invoiceLines(){
        return $this->hasMany(InvoiceLine::class);
    }
    public function status(){
        return $this->belongsTo(Status::class);
    }
    public function reductionScheme(){
        return $this->belongsTo(ReductionScheme::class);
    }
    public function payments(){
        return $this->hasMany(Payment::class);
    }
}
